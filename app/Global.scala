

import play.api.db.DB
import play.api.GlobalSettings
import play.api.Application
import play.api.Play.current
import models.Users
import scala.slick.driver.MySQLDriver.simple._
import Database.threadLocalSession
import models.User
import models.Projects
import scala.slick.jdbc.meta.MTable
import models.ProjectTables

object Global extends GlobalSettings {

  override def onStart(app: Application) {

    lazy val database = Database.forDataSource(DB.getDataSource())

    database.withSession {
      /*if(MTable.getTables.list().size < 4) {
        (Users.component.ddl ++ Projects.component.ddl).create
      }*/
      if (MTable.getTables("USER").list.isEmpty) Users.component.ddl.create
      if (MTable.getTables("PROJECT").list.isEmpty) Projects.component.ddl.create
      //if (MTable.getTables("PROJECT_TABLE").list.isEmpty) ProjectTables.component.ddl.create
    }
  }
}