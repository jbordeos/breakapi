package models

import scala.slick.driver.MySQLDriver.simple._
import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.Play.current
import scala.slick.lifted.ForeignKeyAction
import scala.language.dynamics
//TODO problem : dont know how to generate table with dynamic fields
//Thought of a Solution: Save columns field as String of json.
//Also try to use Dynamic Types in scala - failed
//Proceed with the project by creating new table for ProjectTableValues
case class ProjectTable(id: Option[Long] = None, name: String, projectId: Option[Long] = None,
  columns: Option[String] = None) extends JsonHelper {
  //TODO ERROR
  def getColumns: Seq[TableColumn] = {
    this.columns match {
      case Some(column) =>
        println(">>>>>> \n" + Json.parse(column))
        for { js <- Json.parse(column).as[List[JsObject]] } yield new TableColumn(
          (js \ "name").as[String],
          (js \ "dataType").as[String])
      case None => Seq.empty[TableColumn]
    }
  }
}
case class TableColumn(name: String, dataType: String)
case class ColumnValue(columnName: String, value: String)

trait ProjectTableComponent {
  val ProjectTables: ProjectTables
  class ProjectTables extends Table[ProjectTable]("PROJECT_TABLE") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name", O.NotNull)
    def projectId = column[Long]("projectId", O.NotNull)
    def projectFk = foreignKey("project_id", id, Projects.component)(_.id, onDelete = ForeignKeyAction.Cascade)
    def columns = column[String]("columns")
    def * = id.? ~ name ~ projectId.? ~ columns.? <> (ProjectTable.apply _, ProjectTable.unapply _)
    def autoInc = * returning id
    val byId = createFinderBy(_.id)
  }
}