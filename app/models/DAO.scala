package models

import scala.slick.driver.MySQLDriver.simple.Query
import scala.slick.driver.MySQLDriver.simple.Session
import scala.slick.driver.MySQLDriver.simple.columnExtensionMethods
import scala.slick.driver.MySQLDriver.simple.columnToOrdered
import scala.slick.driver.MySQLDriver.simple.queryToDeleteInvoker
import scala.slick.driver.MySQLDriver.simple.queryToQueryInvoker
import scala.slick.driver.MySQLDriver.simple.stringColumnExtensionMethods
import scala.slick.driver.MySQLDriver.simple.tableQueryToUpdateInvoker
import scala.slick.driver.MySQLDriver.simple.tableToQuery
import scala.slick.driver.MySQLDriver.simple.valueToConstColumn

import play.api.libs.json._
import controllers.AuthenticatedRequest
import scala.slick.jdbc.meta.MTable


case class Page[A](items: Seq[A], page: Int, offset: Long, total: Long) {
  lazy val previous = Option(page - 1).filter(_ >= 0)
  lazy val next = Option(page + 1).filter(_ => (offset + items.size) < total)
}

trait JsonHelper {
  implicit val projectReads = Json.reads[Project]
  implicit val projectWrites = Json.writes[Project]
  implicit val projectFormats = Json.format[Project]
  
  implicit val projectTableReads = Json.reads[ProjectTable]
  implicit val projectTableWrites = Json.writes[ProjectTable]
  implicit val projectTableFormats = Json.format[ProjectTable]
  
  implicit val tableColumnReads = Json.reads[TableColumn]
  implicit val tableColumnWrites = Json.writes[TableColumn]
  implicit val tableColumnFormats = Json.format[TableColumn]
}

private[models] trait DAO extends JsonHelper with UserComponent with ProjectComponent with ProjectTableComponent {
  val Users = new Users
  val Projects = new Projects
  val ProjectTables = new ProjectTables
}

object Users extends DAO {
  def options(implicit s: Session): Seq[(String, String, String)] = {
    val query = (for {
      user <- Users
    } yield (user.id, user.email, user.password)).sortBy(_._2)
    query.list.map(row => (row._1.toString, row._2, row._3))
  }

  def validateUser(user: User)(implicit s: Session): Boolean = {
    Query(Users.where(_.email === user.email).where(_.password === user.password).length).first == 1
  }

  def isUserEmailAvailable(email: String)(implicit s: Session): Boolean = {
    Query(Users.where(_.email === email).length).first == 0
  }

  def getByEmail(email: String)(implicit s: Session): Option[User] = {
    Users.byEmail(email).firstOption
  }

  def insert(user: User)(implicit s: Session): Long = {
    Users.autoInc.insert(user)
  }
  
  def addColumn(implicit s: Session) = {
  }

  def component = Users
}

object Projects extends DAO {

  def findById(id: Long)(implicit s: Session): Option[Project] = {
    Projects.byId(id).firstOption
  }

  def count(implicit s: Session): Int = {
    Query(Projects.length).first
  }

  def count(filter: String)(implicit s: Session): Int = {
    Query(Projects.where(_.name.toLowerCase like filter.toLowerCase).length).first
  }

  def isProjectNameAvailable(name: String)(implicit s: Session, r: AuthenticatedRequest): Boolean = {
    Query(Projects.where(_.name === name).where(_.userId === r.user.id.get).length).first == 0
  }

  def list(page: Int = 0, pageSize: Int = 10, orderBy: Int = 1, filter: String = "%")(implicit s: Session): Page[(Project, Option[User])] = {
    val offset = pageSize * page
    val query =
      (for {
        (project, user) <- Projects leftJoin Users on (_.userId === _.id)
        if project.name.toLowerCase like filter.toLowerCase
      } yield (project, user.id.?, user.email.?, user.password.?))
        .drop(offset)
        .take(pageSize)

    val totalRows = count(filter)
    val result = query.list.map(row => (row._1, row._2.map(value => User(Option(value), row._3.get, row._4.get))))

    Page(result, page, offset, totalRows)
  }

  def listByUser(implicit s: Session, r: AuthenticatedRequest): Seq[Project] = {
    (for {
      project <- Projects if project.userId === r.user.id.get
    } yield (project.id, project.name, project.masterKey, project.userId)).list.map(row => Project(Some(row._1), row._2, Some(row._3), Some(row._4)))
  }

  def insert(project: Project)(implicit s: Session) = {
    Projects.autoInc.insert(project)
  }

  def update(id: Long, project: Project)(implicit s: Session) = {
    val projectToUpdate: Project = project.copy(Some(id))
    Projects.where(_.id === id).update(projectToUpdate)
  }

  def delete(id: Long)(implicit s: Session) = {
    Projects.where(_.id === id).delete
  }

  def component = Projects
}

object ProjectTables extends DAO {
  def listByProject(project: Project)(implicit s: Session): Seq[ProjectTable] = {
    (for {
      pt <- ProjectTables if pt.projectId === project.id
    } yield (pt.id, pt.name, pt.projectId, pt.columns)).list.map(
      row => ProjectTable(Some(row._1), row._2, Some(row._3), Some(row._4)))
  }
  
  def findById(id: Long)(implicit s: Session): Option[ProjectTable] = {
    ProjectTables.byId(id).firstOption
  }
  
  def insert(projectTable: ProjectTable)(implicit s: Session) = {
    val ptToInsert: ProjectTable = new ProjectTable(projectTable.id, 
        projectTable.name, projectTable.projectId, Some(Json.toJson(projectTable.getColumns).toString))
    ProjectTables.autoInc.insert(ptToInsert)
  }
  
  def update(id: Long, projectTable: ProjectTable)(implicit s: Session) = {
    val ptToUpdate: ProjectTable = projectTable.copy(Some(id))
    ProjectTables.where(_.id === id).update(ptToUpdate)
  }
  
  def delete(id: Long)(implicit s: Session) = {
    ProjectTables.where(_.id === id).delete
  }
  
  def isProjectTableNameAvailable(name: String, project: Project)(implicit s: Session, r: AuthenticatedRequest): Boolean = {
    //if (MTable.getTables("PROJECT_TABLE").list.isEmpty) ProjectTables.ddl.createStatements
    Query(ProjectTables.where(_.name === name).where(_.projectId === project.id).length).first == 0
  }
  
  def component = ProjectTables
}
