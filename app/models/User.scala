package models

import scala.slick.driver.MySQLDriver.simple._
import java.util.Date
import play.api.Play.current
import play.api.libs.json._
import slick.lifted.{ Join, MappedTypeMapper }
import scala.slick.lifted.ForeignKeyAction
import controllers.AuthenticatedRequest


case class User(id: Option[Long] = None, email: String, password: String) {
  def getProjects(implicit s: Session, r: AuthenticatedRequest) = {Projects.listByUser}
}

trait UserComponent {
  val Users: Users
  class Users extends Table[User]("USER") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def email = column[String]("email")
    def password = column[String]("password")
    def * = id.? ~ email ~ password <> (User.apply _, User.unapply _)
    def autoInc = * returning id
    def byEmail = createFinderBy(_.email)
  }
}
