package models

import scala.slick.driver.MySQLDriver.simple._
import java.util.Date
import play.api.Play.current
import slick.lifted.{ Join, MappedTypeMapper }
import scala.slick.lifted.ForeignKeyAction
import play.api.libs.json._
import java.util.UUID

case class Project(id: Option[Long] = None, name: String, masterKey: Option[String] = Some(UUID.randomUUID.toString), userId: Option[Long] = None) {
  def getProjectTables(implicit s: Session) = {
    ProjectTables.listByProject(this)
  }
}

trait ProjectComponent {
  val Projects : Projects
  class Projects extends Table[Project]("PROJECT") {
    implicit val javaUtilDateTypeMapper = MappedTypeMapper.base[java.util.Date, java.sql.Date](
      x => new java.sql.Date(x.getTime),
      x => new java.util.Date(x.getTime)
    )
    
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name", O.NotNull)
    def masterKey = column[String]("masterKey", O.NotNull)
    def userId = column[Long]("userId", O.NotNull)
    def userFk = foreignKey("user_id", id, Users.component)(_.id, onDelete = ForeignKeyAction.Cascade)
    
    def * = id.? ~ name ~ masterKey.? ~ userId.? <> (Project.apply _, Project.unapply _)
    
    def autoInc = * returning id
    
    val byId = createFinderBy(_.id)
    
  }
}