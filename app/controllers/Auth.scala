package controllers

import play.api._
import play.api.mvc._
import play.api.data.Form
import play.api.data.Forms._
import views._
import models.User
import views.html.defaultpages.unauthorized
import scala.slick.driver.MySQLDriver.simple._
import Database.threadLocalSession
import play.api.db.DB
import play.api.Play.current
import models.Users

case class AuthenticatedRequest(
  val user: User, request: Request[AnyContent]) extends WrappedRequest(request)

object Auth extends AiController {

  val loginForm = Form(
    tuple(
      "email" -> text,
      "password" -> text) verifying ("Invalid email or password", result => result match {
        case (email, password) => database withSession {
          Users.validateUser(User(None, email, password))
        }
      }))

  def check(username: String, password: String) = {
    (username == "admin@admin.com" && password == "admin")
  }

  def login = Action { implicit request =>
    Ok(html.login(loginForm, UserController.userForm))
  }

  def authenticate = Action { implicit request =>
    loginForm.bindFromRequest.fold(
      formWithErrors => BadRequest(html.login(formWithErrors, UserController.userForm)),
      user =>
        Redirect(routes.Application.index).withSession(Security.username -> user._1))
  }

  def logout = Action {
    Redirect(routes.Auth.login).withNewSession.flashing(
      "success" -> "You are now logged out.")
  }
}

trait Secured {
  
  lazy val database = Database.forDataSource(DB.getDataSource())
  
  def username(request: RequestHeader) = request.session.get(Security.username)

  def onUnauthorized(request: RequestHeader) = Results.Redirect(routes.Auth.login)

  def Authenticated(f: AuthenticatedRequest => Result) = {
    Action { request =>
      username(request).flatMap(u => database withSession { Users.getByEmail(u) }).map {
        user =>
          f(AuthenticatedRequest(user, request))
      }.getOrElse(onUnauthorized(request))
    }
  }
}