package controllers

import play.api._
import play.api.mvc._
import scala.slick.driver.MySQLDriver.simple._
import Database.threadLocalSession
import models.Projects
import play.api.libs.json._
import models.ProjectTableComponent
import models.TableColumn
import models.ProjectTables
import models.ProjectTable
import scala.slick.jdbc.meta.MTable
import models.Users
object Application extends AiController {
  
  def index = Authenticated { implicit request =>
    val column1 = new TableColumn("client", "String")
    val column2 = new TableColumn("org", "Long")
    val cols = Seq(column1, column2)
    database withSession {
      //val ptU = new ProjectTable(None, "TableSample", Some(1L), None)
      //ProjectTables.insert(ptU, cols)
      //println("======")
      //println(ProjectTables.listByProject(projects.head).head.getColumns)
    }
    
    Ok(views.html.index(ProjectController.projectForm))
  }

}