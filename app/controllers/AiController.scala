package controllers

import play.api.mvc._
import scala.slick.driver.MySQLDriver.simple.Database.threadLocalSession
import scala.slick.driver.MySQLDriver.simple._
import play.api.db._
import play.api.Play.current
import java.util.Properties
import play.api.Play
import models.JsonHelper
import models.Project
import play.api.cache.Cache
import models.Projects
import models.ProjectTables
import scala.slick.jdbc.meta.MTable
trait AiController extends Controller with Secured with JsonHelper {
  def databaseConnect(dbName: String) = {
    val conf = Play.application.configuration
    val dr = conf.getString("db.default.driver").get
    val us = conf.getString("db.default.user").get
    val pa = conf.getString("db.default.password").get
    val urlArray = conf.getString("db.default.url").get.split("\\?")
    val modifiedUrl = urlArray(0).substring(0, 23) + dbName + "?" + urlArray(1)
    println("url : " + modifiedUrl)
    Database.forURL(modifiedUrl, driver = dr, user = us, password = pa)
  }

  def setProjectCache(projectId: Long)(implicit request: AuthenticatedRequest) = {
    Cache.set("project|" + request.user.id, database withSession { Projects.findById(projectId).get })
  }
  def getProjectCache(implicit request: AuthenticatedRequest): Option[Project] = {
    Cache.getAs[Project]("project|" + request.user.id)
  }
  def clearProjectCache(implicit request: AuthenticatedRequest) = Cache.remove("project|" + request.user.id)
  def getTargetDB(project: Project)(implicit request: AuthenticatedRequest): String = {
    val target = project.name + "_" + request.user.id.get
    databaseConnect(target) withSession {
      if (MTable.getTables("PROJECT_TABLE").list.isEmpty) ProjectTables.component.ddl.create
    }
    target
  }
}