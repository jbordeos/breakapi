package controllers

import play.api._
import play.api.mvc._
import play.api.data.Form
import play.api.data._
import play.api.data.Forms._
import scala.slick.driver.MySQLDriver.simple._
import Database.threadLocalSession
import scala.slick.session.Session
import play.api.libs.json._
import play.api.db._
import play.api.Play.current
import models.User
import models.Users
import views._

object UserController extends AiController {

  val userForm = Form(
    mapping(
      "email" -> text,
      "password" -> text)((email, password) => User(None, email, password))((user: User) => Some(user.email, user.password))
      verifying ("Email is already used.",
        result => result match {
          case (user) => database withSession {
            Users isUserEmailAvailable (user.email)
          }
        }))

  def addUser = Action { implicit request =>
    userForm.bindFromRequest.fold(
      formWithErrors => {
        BadRequest(html.login(Auth.loginForm, formWithErrors))
      },
      user => {
        database withSession {
          Users insert user
        }
        Redirect(routes.Auth.login)
      })
  }

  def getUsers = Authenticated { implicit request =>
    val json = database withSession {
      val users = for (u <- Users.component) yield u.email
      Json.toJson(users.list)
    }
    Ok(json).as(JSON)
  }

}