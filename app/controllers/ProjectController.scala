package controllers

import scala.slick.driver.MySQLDriver.simple._
import scala.slick.driver.MySQLDriver.simple.Database.threadLocalSession
import scala.slick.session.Session
import models.Project
import models.Projects
import play.api._
import play.api.Play.current
import play.api.data._
import play.api.data.Form
import play.api.data.Forms._
import play.api.mvc._
import views._
import scala.slick.jdbc.meta.MTable
import models.Users
import play.api.cache._
import models.ProjectTable
import models.ProjectTables
import play.api.libs.json.Json

object ProjectController extends AiController {

  def projectForm(implicit request: AuthenticatedRequest) = Form(
    mapping(
      "name" -> text)((name) => Project(None, name))((project: Project) => Some(project.name)) verifying ("Project Name must not already be used in your account.",
        result => result match {
          case (project) => database withSession {
            Projects.isProjectNameAvailable(project.name)
          }
        }))
  def projectTableForm(implicit request: AuthenticatedRequest) = Form(
    mapping(
      "name" -> text)((name) => ProjectTable(None, name))((projectTable: ProjectTable) => Some(projectTable.name)) verifying ("ProjectTable Name must not already be used in the Project.",
        result => result match {
          case (projectTable) =>
            getProjectCache match {
              case Some(project) =>
                databaseConnect(getTargetDB(project)) withSession { ProjectTables.isProjectTableNameAvailable(projectTable.name, project) }
              case None =>
                false
            }
        }))

  def index(projectId: Long) = Authenticated {
    implicit request =>
      setProjectCache(projectId)
      Ok(html.project_index(ProjectController.projectTableForm))
  }

  def getProjectsAsJson = Authenticated { implicit request =>
    database withSession {
      val json = Json.toJson(Projects.listByUser)
      Ok(json).as(JSON)
    }
  }

  def getProjectTablesAsJson = Authenticated { implicit request =>
    getProjectCache match {
      case Some(project) => databaseConnect(getTargetDB(project)) withSession {
        val json = Json.toJson(ProjectTables.listByProject(project))
        Ok(json).as(JSON)
      }
      case None =>
        Ok("")
    }
  }

  def addProject = Authenticated { implicit request =>
    projectForm.bindFromRequest.fold(
      formWithErrors => {
        BadRequest(html.index(formWithErrors))
      },
      project => {
        database withSession {
          println(request.user)
          val updatedProj = Project(project.id, project.name, project.masterKey, Some(request.user.id.get))
          Projects insert updatedProj
        }
        Redirect(routes.Application.index).flashing(
          "success" -> ("Created new Project : " + project.name))
      })
  }

  def addProjectTable = Authenticated { implicit request =>
    projectTableForm.bindFromRequest.fold(
      formWithErrors => {
        BadRequest(html.project_index(formWithErrors))
      },
      projectTable => {
        getProjectCache match {
          case Some(project) =>
            databaseConnect(getTargetDB(project)) withSession {
              println("+____+ " + project.id)
              val data = ProjectTable(projectTable.id, projectTable.name, project.id, None)
              ProjectTables.insert(data)
            }
            Redirect(routes.ProjectController.index(project.id.get)).flashing(
              "success" -> ("Created new Project : " + project.name))
          case None => Redirect(routes.Auth.login)
        }
      })
  }
  
  def addProjectTableColumn = Authenticated { implicit request =>
    //TODO
    Ok
  }
}