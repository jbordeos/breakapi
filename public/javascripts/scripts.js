$(document).ready(function(){
	setProjectsAsLI($('ul#projects'))
	setProjectTablesAsLI($('ul#projectTables'))
})

function setProjectsAsLI(ul) {
  $(ul).html('<div class="gifLoader"></div>')
  $.ajax({
	  type: "GET",
	  dataType: "json",
	  url: "/projects/json",
	  success: function (d) {
		  var li = ""
		  $.each(d, function(i, p){
			li += "<li><a href='/projects/index/" + p.id + "'>"+p.name+"</a></li>"  
		  })
		  $(ul).html(li)
	  }
  })
}

function setProjectTablesAsLI(ul) {
  $(ul).html('<div class="gifLoader"></div>')
  $.ajax({
	type: "GET",
	dataType: "json",
    url: "/project/tables/json",
	success: function (d) {
	  var li = ""
	  $.each(d, function(i, p){
		li += "<li value='"+p.id+"'>"+p.name+"</li>"  
	  })
	  $(ul).html(li)
	}
  })
}