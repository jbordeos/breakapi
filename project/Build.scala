import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName = "AiCoreApi"
  val appVersion = "1.0-SNAPSHOT"

  val appDependencies = Seq(
    // Add your project dependencies here,
    jdbc,
    anorm,
    "com.typesafe.slick" %% "slick" % "1.0.1",
    "com.typesafe.play" %% "play-slick" % "0.4.0" ,
    "org.scalatest" % "scalatest_2.10.0-M7" % "2.0.M4-2.10.0-M7-B1" % "test",
    "org.scala-lang" % "scala-actors" % "2.10.0-M7" % "test",
    "mysql" % "mysql-connector-java" % "5.1.21",
    "com.typesafe" % "slick_2.10.0-M7" % "0.11.1")

  val main = play.Project(appName, appVersion, appDependencies).settings( // Add your own project settings here
      testOptions in Test := Nil
  )

}
